export const initialState = {
	token: '',
	errorMessage: null,
};

export const AuthReducer = (initialState, action) => {
	switch (action.type) {
		case 'LOGIN_SUCCESS':
			return {
				...initialState,
				token: action.payload.jwt,
			};
		case 'LOGOUT':
			return {
				...initialState,
				user: '',
				token: '',
			};

		case 'LOGIN_ERROR':
			return {
				...initialState,
				errorMessage: action.error,
			};

		default:
			return initialState
	}
};