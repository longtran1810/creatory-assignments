import {API_URL} from '../../constants/endpoint';
import callApi from '../../services/callApi';

export async function loginUser (payload, dispatch) {
  try {
    const response = await callApi (`${API_URL.auth.signin}`, {
      method: 'POST',
      body: payload,
    });
    if (response.authenticated) {
      localStorage.setItem ('user-tk', response.jwt);
      dispatch ({type: 'LOGIN_SUCCESS', payload: response});

      return response.jwt;
    }
  } catch (err) {
    console.log(err, 'no')
    return "";
  }
}

export async function checkJwt (payload, dispatch) {
  try {

    const response = await callApi (`${API_URL.auth.checkJWT}`, {
      method: 'POST',
      headers: {
        jwt: payload,
      },
    });
    if (response.authenticated) {
      dispatch ({type: 'LOGIN_SUCCESS', payload: {...response, jwt: payload}});
    }
  } catch (err) {
    return;
  }
}

export function logout () {
  localStorage.removeItem ('user-tk');
}

export function getJWT () {
  return localStorage.getItem ('user-tk');
}
