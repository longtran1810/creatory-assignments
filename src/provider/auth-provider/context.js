import React, {useEffect, useReducer, useState} from 'react';
import {checkJwt, getJWT} from './actions';
import {initialState, AuthReducer} from './reducer';

const AuthStateContext = React.createContext ();
const AuthDispatchContext = React.createContext ();

export function useAuthState () {
  const context = React.useContext (AuthStateContext);
  if (context === undefined) {
    throw new Error (
      'context is undefined!!! "useAuthState" must be used within a AuthProvider'
    );
  }

  return context;
}

export function useAuthDispatch () {
  const context = React.useContext (AuthDispatchContext);
  if (context === undefined) {
    throw new Error (
      'context is undefined!!! "useAuthDispatch" must be used within a AuthProvider'
    );
  }

  return context;
}

export const AuthProvider = ({children}) => {
  const [user, dispatch] = useReducer (AuthReducer, initialState);
  const [loading, setLoading] = useState (true);
  useEffect (() => {
    checkAuth ();

    async function checkAuth () {
      const jwt = getJWT ();

      if (!jwt) {
        setLoading (false);
        return;
      }
      await checkJwt (jwt, dispatch);
      setLoading (false);
    }
  }, []);
  return (
    !loading &&
    <AuthStateContext.Provider value={user}>
      <AuthDispatchContext.Provider value={dispatch}>
        {children}
      </AuthDispatchContext.Provider>
    </AuthStateContext.Provider>
  );
};
