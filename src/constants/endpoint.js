export const API_URL = {
  domain: "http://localhost:8888",
  auth: {
    signin: "/auth",
    checkJWT: "/check-jwt"
  },
  user: {
    list: "/data",
  },
};