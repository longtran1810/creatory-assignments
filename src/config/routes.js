import React from 'react';

const Login = React.lazy(() => import("../pages/Login"))
const UserManagement = React.lazy(() => import("../pages/UserMgn/index"))
const NotFound = React.lazy(() => import("../pages/NotFound"))

const routes = [
  {
    path: '/login',
    component: Login,
    isPrivate: false,
  },
  {
    path: '/user-management',
    component: UserManagement,
    isPrivate: true,
  },
  {
    path: '/*',
    component: NotFound,
    isPrivate: true,
  },
];

export default routes;
