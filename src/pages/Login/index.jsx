import React from 'react'
import { Formik, Form } from 'formik';
import * as Yup from 'yup';

import { Row, Col, Input, Button } from '../../components/base'
import { LayoutCommon } from '../../components/layout/LayoutCommon';
import loginStyle from './login.module.scss';
import { loginUser } from '../../provider/auth-provider/actions';
import { useAuthDispatch } from '../../provider/auth-provider';

function Login(props) {
  const { history } = props;
  const dispatch = useAuthDispatch();
  const initialValues = {
    username: '',
    password: ''
  };
  const validationSchema = Yup.object().shape({
    username: Yup.string()
      .required('Email is required'),
    password: Yup.string().required('Password is required')
  });

  async function onSubmit({ username, password }, { setSubmitting }) {
    try {
      const response = await loginUser({ username, password }, dispatch);
      if (response) {
        history.push("/user-management")
      }
    } catch {
      return;
    } finally {
      setSubmitting(false);
    }
  }
  return (
    <LayoutCommon>
      <Row gutter={0} className={loginStyle.loginContainer}>
        <Col span={6}>
          <Formik initialValues={initialValues} validationSchema={validationSchema} onSubmit={onSubmit}>
            {({ errors, touched, handleChange, isSubmitting }) => (
              <Form className={loginStyle.formLogin} noValidate>
                <div className="form-field">
                  <h3>Username</h3>
                  <Input name="username" type="text" onChange={handleChange} placeholder="username" inValid={!!(touched.username && errors.username)} />
                  {touched.username && errors.username && (
                    <div className={loginStyle.invalidMsg}>{errors.username}</div>
                  )}
                </div>
                <div className="form-field">
                  <h3>Password</h3>
                  <Input name="password" type="password" onChange={handleChange} placeholder="password" inValid={!!(touched.password && errors.password)} />
                  {touched.password && errors.password && (
                    <div className={loginStyle.invalidMsg}>{errors.password}</div>
                  )}
                </div>
                <div className={loginStyle.submitBtn}>
                  <Button type="submit" disabled={isSubmitting}>Login</Button>
                </div>
              </Form>
            )}
          </Formik>
        </Col>
      </Row>
    </LayoutCommon>
  )
}

export default Login;
