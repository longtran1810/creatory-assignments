import React, {useState, useEffect} from 'react';
import {LayoutCommon} from '../../components/layout/LayoutCommon';
import {getUserMgnData} from '../../services/user.service';
import usermgnS from './usermgn.module.scss';

const Table = React.lazy(() => import("../../components/table"));
const ModalUser = React.lazy(() => import("./ModalUser"))
const UserManagement = ({userAuth = {}}) => {
  const [list, setList] = useState ({arr: [], totalPages: 0, firstLoad: true});
  const [modalState, setModalState] = useState({user: {}, open: false});
  const columns = [
    {
      field: 'id',
      headerName: 'Id',
    },
    {
      field: 'firstName',
      headerName: 'Name',
    },
    {
      field: 'phone',
      headerName: "Phone"
    },
    {
      field: 'email',
      headerName: 'email',
    },
    {
      field: 'isActive',
      headerName: 'status',
      type: 'badge',
    },
  ];

  useEffect (() => {
    getDataTablePagin ({page: 0, numberPerPage: 10});
  }, []);
  async function getDataTablePagin({page = 0, numberPerPage = 10}) {
    const userData = await getUserMgnData (page + 1, numberPerPage, userAuth);
    setList (prev => ({
      ...prev,
      arr: userData.user,
      totalPages: userData.filter.totalUser,
      firstLoad: false,
    }));
  };

  const onTouchRow = (user) => {
    setModalState((prev) => ({...prev, user, open: true}))
  }
  const onCloseModal = () => {
    setModalState((prev) => ({...prev, user: Object.assign({}), open: false}))
  }
  return (
    <LayoutCommon>
      <div className={`${usermgnS.tableWrap} `}>
        {!list.firstLoad &&
          <Table
            firstLoad={list.firstLoad}
            columns={columns}
            list={list.arr}
            onGetData={getDataTablePagin}
            total={list.totalPages}
            onTouchRow={onTouchRow}
          />}
      </div>
      {modalState.open && (<ModalUser user={modalState.user} onCloseModal={onCloseModal} />)}
    </LayoutCommon>
  );
};

export default UserManagement;
