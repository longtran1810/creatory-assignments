import React from 'react';
import {Col, Row} from '../../../components/base';
import {Modal} from '../../../components/modal';
import femaleImg from '../../../assets/images/female.png';
import maleImg from '../../../assets/images/male.png';
import modalUserS from './modaluser.module.scss';
import { formatCurrency } from '../../../helpers/format';

const ModalUser = ({user = {}, onCloseModal = () => {}}) => {
  return (
    <Modal>
      <div className={modalUserS.modalHeader}>
        Profile of {user.firstName} {user.lastName}
      </div>
      <Row className={modalUserS.modalContent}>
        <Col span={4}>
          <img src={`${user.gender === 'female' ? femaleImg : maleImg}`} />
        </Col>
        <Col span={8}>
          <div className={modalUserS.descriptionWrap}>
            <span><b>Profile Id:</b> {user.guid}</span>
            <span><b>Age: </b> {user.age}</span>
            <span><b>Phone No:</b> {user.phone}</span>
            <span><b>Balance:</b> {formatCurrency(user.balance)}</span>
          </div>
        </Col>
      </Row>
      <div className={modalUserS.modalFooter}>
        <button onClick={onCloseModal}>Close</button>
      </div>
    </Modal>
  );
};

export default ModalUser;
