import React, { Suspense } from 'react';
import { Router, Switch } from 'react-router-dom';
import './App.css';
import AppRoute from './components/app-route';
import routes from './config/routes';
import { history } from './helpers/history';
import { AuthProvider } from './provider/auth-provider';

function App() {
  return (
    <AuthProvider>
      <Suspense fallback={false}>
        <Router history={history}>
          <Switch>
            {routes.map((route) => (
              <AppRoute
                key={route.path}
                path={route.path}
                component={route.component}
                isPrivate={route.isPrivate}
                {...route}
              />
            ))}
          </Switch>
        </Router>
      </Suspense>
    </AuthProvider>
  );
}

export default App;
