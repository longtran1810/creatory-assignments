import fetch from 'isomorphic-fetch';
import { API_URL } from '../constants/endpoint';

const initialOption = {
  method: 'GET',
};

export default async function callApi(endpoint, option = initialOption) {
  try {
    const options = {
      method: option.method,
      body: option.body && JSON.stringify(option.body),
      headers: {
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        ...option.headers
      },
    };
    const res = await fetch(`${API_URL.domain}${endpoint}`, options);
    const resData = await res.json();

    if (res.ok) {
      if (res.status === 200) {
        return resData;
      }
    }
    throw resData;
  } catch (err) {
    // catch condition here
    throw err;
  }
}
