import {API_URL} from '../constants/endpoint';
import callApi from './callApi';

export async function getUserMgnData (page = 1, limit = 10, userAuth = {}) {
  try {
    const res = await callApi (
      `${API_URL.user.list}?page=${page}&limit=${limit}`,
      {
        headers: {
          jwt: userAuth.token,
        },
      }
    );
    if (res.statusCode === 200) {
      return res.data;
    }
    return {};
  } catch (err) {
    return {};
  }
}
