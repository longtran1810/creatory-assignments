import React from 'react';
import {Badge} from '../base';

const TableCell = ({type, data}) => {
  switch (type) {
    case 'badge':
      return (
        <td style={{display: 'flex', justifyContent: 'center'}}>
          <Badge type={data ? 'active' : 'inActive'}>
            {data ? 'Active' : 'In-Active'}
          </Badge>
        </td>
      );
    default:
      return <td><span>{data}</span></td>;
  }
};

export default TableCell;
