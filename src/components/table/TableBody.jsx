import React from "react";
import TableCell from "./TableCell";

const TableBody = ({ data, columnsData, onTouchRow }) => (
  <tbody>
    {data?.map((item, i) => (
      <tr key={i} onClick={() => onTouchRow(item)}>
        {columnsData?.map((col, j) =>
          <TableCell key={j} data={item[col.field]} type={col.type} />
        )}
      </tr>
    ))}
  </tbody>
)

export default TableBody;