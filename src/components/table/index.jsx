import React, {useEffect, useState} from 'react';
import TableHeader from './TableHeader';
import TableBody from './TableBody';
import TableFooter from './TableFooter';
import './style.scss';

const Table = props => {
  const {
    columns,
    list,
    firstLoad,
    total,
    numberPerPage: numberPerPageTable,
    isRtl = false,
    numberPageOfText,
    tableClassName,
    onGetData,
    onTouchRow = () => {},
  } = props;
  const [data, setData] = useState ([]);
  const [columnsData, setColumnsData] = useState ([]);
  const [orderby, setOrderBy] = useState ('');
  const [order, setOrder] = useState ('');
  const [totalTable, setTotalTable] = useState (1);
  const [page, setPage] = useState (1);
  const [numberPerPage, setNumberPerPage] = useState (10);
  const [numberOfPage, setNumberOfPage] = useState (0);

  useEffect (() => {
    setColumnsData (columns);
    setTotalTable (total);
  }, [total, columns]);

  useEffect (
    () => {
      setNumberPerPage (numberPerPageTable ? numberPerPageTable : 10);
      setNumberOfPage (Math.ceil (totalTable / numberPerPage));
    },
    [totalTable]
  );

  useEffect (
    () => {
      setNumberPerPage (numberPerPage ? numberPerPage : 10);
      setNumberOfPage (Math.ceil (totalTable / numberPerPage));
    },
    [numberPerPage]
  );

  useEffect (
    () => {
      setData (list);
    },
    [list]
  );

  useEffect (
    () => {
      if (!firstLoad && totalTable !== 1) {
        onGetData ({
          page: page - 1,
          numberPerPage,
          order: order == 'descending' ? 'ascending' : 'descending',
          orderby,
          data,
        });
      }
    },
    [page, numberPerPage]
  );

  const sort = item => {
    setOrderBy (item);
    let orderTemp = 'descending';
    if (item === orderby && order === 'descending') {
      orderTemp = 'ascending';
    }

    if (orderTemp === 'descending') {
      setData (
        list
          .sort ((a, b) => (a[item] > b[item] ? 1 : -1))
          .slice ((page - 1) * numberPerPage, page * numberPerPage)
      );
    } else {
      setData (
        list
          .sort ((a, b) => (a[item] < b[item] ? 1 : -1))
          .slice ((page - 1) * numberPerPage, page * numberPerPage)
      );
    }
    setOrder (orderTemp);
  };

  const nextPage = wishPage => {
    if (wishPage) {
      if (wishPage + 1 <= numberOfPage) {
        setPage (wishPage);
      }
      return;
    }
    if (page + 1 <= numberOfPage) {
      setPage (page + 1);
    }
  };

  const previousPage = () => {
    if (page - 1 >= 1) {
      setPage (page - 1);
    }
  };

  const handleChange = e => {
    setNumberPerPage (e.target.value);
    setPage (1);
  };

  return (
    <div className={isRtl ? 'rtl' : 'ltr'}>
      <div className="">
        <table className={tableClassName ? tableClassName : 'mytable'}>
          <TableHeader
            columns={columns}
            orderby={orderby}
            order={order}
            sort={sort}
          />
          <TableBody data={data} columnsData={columnsData} onTouchRow={onTouchRow}/>
        </table>
      </div>
      <TableFooter
        previousPage={previousPage}
        nextPage={nextPage}
        isRtl={isRtl}
        page={page}
        numberOfPage={numberOfPage}
        numberPerPage={numberPerPage}
        handleChange={handleChange}
        numberPageOfText={numberPageOfText}
      />
    </div>
  );
};

export default Table;
