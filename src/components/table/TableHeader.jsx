import React from 'react';

const TableHeader = ({columns, orderby, order, sort}) => {
  return (
    <thead>
      <tr>
        {columns &&
          columns.map ((item, i) => (
            <th
              key={i}
              onClick={() => (item.field !== '' ? sort (item.field) : '')}
            >
              {item.headerName}
              {orderby === item.field
                ? order === 'descending'
                    ? <i className="up-caret"> &#9650;</i>
                    : <i className="down-caret">&#9660;</i>
                : ''}
            </th>
          ))}
      </tr>
    </thead>
  );
};

export default TableHeader;
