import React, {useEffect, useState} from 'react';
import {useDebounce} from '../../hooks/useDebounce';

const numberInPages = [5, 10, 20, 50, 100];

const TableFooter = ({
  previousPage,
  nextPage,
  isRtl = false, // more
  page,
  numberOfPage,
  numberPerPage,
  handleChange,
  numberPageOfText,
}) => {
  const [pageNum, setPageNum] = useState (1);
  const onChangePage = e => {
    setPageNum (e.target.value);
  };
  const debouncePageChange = useDebounce (pageNum, 200);
  useEffect (
    () => {
      nextPage(parseInt(debouncePageChange))
    },
    [debouncePageChange]
  );
  return (
    <span className="footer-item">
      <span className="" onClick={() => previousPage ()}>
        <i className="chevron">&#x3c;</i>
      </span>
      <span className="" onClick={() => nextPage ()}>
        <i className="chevron">&#x3e;</i>
      </span>
      <div className="number-of-page-text">
        {page &&
          <input
            type="number"
            min={0}
            value={page}
            onChange={onChangePage}
          />}
        <span> {numberPageOfText ? numberPageOfText : 'of'} </span>
        <span> {numberOfPage} </span>
      </div>

      <div className="number-of-list">
        <select value={numberPerPage} onChange={event => handleChange (event)}>
          {numberInPages.map ((option, i) => <option key={i}>{option}</option>)}
        </select>
      </div>
    </span>
  );
};

export default TableFooter;
