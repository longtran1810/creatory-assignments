import React from 'react'
import modalS from './modal.module.scss';

export const Modal = ({children}) => {
  return (
    <div className={modalS.modalWrapper}>
      <div className={modalS.modalInner}>
        {children}
      </div>
    </div>
  )
}
