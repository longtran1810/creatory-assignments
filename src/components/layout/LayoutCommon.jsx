import React from 'react'
import layoutS from './layoutcommon.module.scss';

export const LayoutCommon = ({children}) => {
  return (
    <div className={layoutS.wrapper}>
      {children}
    </div>
  )
}
