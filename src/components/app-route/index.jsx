import React from 'react';
import {Redirect, Route} from 'react-router-dom';
import {useAuthState} from '../../provider/auth-provider';

function AppRoute({component: Component, path, isPrivate, ...rest}) {
  const userAuth = useAuthState ();
  return (
    <Route
      {...rest}
      path={path}
      render={props => {
        if (isPrivate && !Boolean (userAuth.token)) {
          return (
            <Redirect
              from={path}
              to={{pathname: '/login', state: {from: props.location}}}
            />
          );
        }
        return <Component {...props} userAuth={userAuth} />;
      }}
    />
  );
}

export default AppRoute;
