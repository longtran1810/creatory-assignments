import React from 'react'
import inputStyle from './style/input.module.scss'

export const Input = (props) => {
  return (
    <div className={`${inputStyle.inputWrapper} ${props.inValid ? inputStyle.inValid : ''}`}>
      <input {...props}/>
    </div>
  )
}
