import React, { Children } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import s from './style/rowcol.module.scss';

function Row(props) {
  const { justify, align, className, gutter, style, children,
    prefix = 'vct-grid-row', ...others } = props;
  const classes = cx(s[`${prefix}`], {
    [s[`${prefix}-${justify}`]]: justify,
    [s[`${prefix}-${align}`]]: align,
  }, className);
  const rowStyle = gutter > 0 ? Object.assign({}, {
    marginLeft: gutter / -2,
    marginRight: gutter / -2,
  }, style) : style;
  const cols = Children.map(children, (col) => {
    if (!col) {
      return null;
    }
    if (col.props && gutter > 0) {
      return React.cloneElement(col, {
        style: Object.assign({}, {
          paddingLeft: gutter / 2,
          paddingRight: gutter / 2,
        }, col.props.style),
      });
    }
    return col;
  });

  return <div {...others} className={classes} style={rowStyle}>{cols}</div>;
}

Row.defaultProps = {
  gutter: 0,
  justify: 'start',
  align: 'top'
}

Row.propTypes = {
  align: PropTypes.string,
  justify: PropTypes.string,
  className: PropTypes.string,
  children: PropTypes.node,
  gutter: PropTypes.number,
  prefix: PropTypes.string,
  style: PropTypes.object,
  noGutter: PropTypes.bool
};

export { Row };