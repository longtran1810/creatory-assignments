import React from 'react';
import badgeStyle from './style/badge.module.scss';

export const Badge = ({type, children}) => {
  return (
    <div className={`${badgeStyle.badge} ${badgeStyle[type]}`}>
      {children}
    </div>
  );
};
