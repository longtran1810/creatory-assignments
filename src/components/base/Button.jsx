import React from 'react';
import Proptypes from "prop-types";

import buttonStyle from './style/button.module.scss'

export const Button = (props) => {
  return (
    <button {...props} className={buttonStyle.buttonWrapper}>
      {props.children}
    </button>
  )
}

Button.defaultProps = {
  type: 'button',
}
Button.propTypes = {
  type: Proptypes.string.isRequired,
  onClick: Proptypes.func,
  disabled: Proptypes.bool
}
