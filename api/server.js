const express = require ('express');
const bodyParser = require ('body-parser');
const cors = require ('cors');

const port = 8888; // use env later
const app = express ();
const data = require ('./data.json');
const sampleJWT = 'JWTLater';

app.use (cors ({credentials: true, origin: ['http://localhost:3000']}));
app.use (bodyParser.json ());

app.post ('/auth', (req, res) => {
  const username = req.body.username;
  const password = req.body.password;

  if (username === 'admin' && password === 'admin') {
    res.json ({authenticated: true, jwt: sampleJWT});
  } else {
    res.status (401).send ({authenticated: false});
  }
});

app.post ('/check-jwt', (req, res) => {
  // temporarily check
  const reqHeader = req.headers || {};
  if (reqHeader.jwt !== sampleJWT) {
    res.status (403).send ({authenticated: false});
  }
  return res.status (200).send ({authenticated: true});
});

app.get ('/data', (req, res) => {
  const reqHeader = req.headers || {};
  if (reqHeader.jwt !== sampleJWT) {
    res.status (403).send ({authenticated: false});
  }
  const {page = 1, limit = 10} = req.query || {};
  const pageInt = parseInt (page), limitInt = parseInt (limit);
  //
  const start = (pageInt - 1) * limitInt;
  const end = pageInt * limitInt;

  const userData = data.slice (start, end);
  res.status (200).send ({
    statusCode: 200,
    data: {
      user: userData,
      filter: {
        totalUser: data.length,
      },
    },
  });
});

app.listen (port, () => {
  console.log (
    `Creatory frontend assignment dev server is running in port ${port}:)`
  );
});
